from __future__ import division
from netcloxync.framework.managers import ClockManager
from netcloxync.framework.managers import EventManager
from netcloxync.framework.managers import NetworkManager
from math import floor


class AverageTimeSync(object):
    algname = 'AverageTimeSync'

    def __init__(self, nodeid, skew, offset, **kwargs):
        self.id = nodeid
        self.clock = ClockManager(skew, offset)
        self.net_manager = NetworkManager(self, nodeid)
        self.event_manager = EventManager(self.clock)

        # Algorithm Parameters as initialised by the test.
        self.sync_int = kwargs['sync_int']
        self.rho_eta = kwargs['rho_eta']
        self.rho_alpha = kwargs['rho_alpha']
        self.rho_nu = kwargs['rho_nu']

        # Create any instance variables needed for computation
        self.ats_data = {}
        self.tau_hat_i = self.clock.time()
        self.alpha_hat_i = 1.0
        self.nu_hat_i = 0
        self.tau_i_ref = self.clock.time()
        self.tau_hat_i_ref = self.tau_i_ref

        # Start ATS Algorithm
        self.event_manager.add_timed_event(self.sync_int, self.sync_int_expired)

    def sync_int_expired(self):
        """
        Each time sync_int_expired is triggered, this node will transmit a
        packet to all of its neighbours, and then requeue the sync_int_expired
        event.
        """
        time_sent = self.clock.time()
        atspacket = {'id': self.id,
                     'tau': time_sent,
                     'alpha_hat': self.alpha_hat_i,
                     'tau_ref': self.tau_i_ref,
                     'tau_hat_ref': self.tau_hat_i_ref,}

        for n in self.net_manager.neighbours():
            self.net_manager.send_packet(n, atspacket)
        self.event_manager.add_timed_event(self.sync_int, self.sync_int_expired)

    def recv_packet(self, packet):
        time_received = self.clock.time()
        tau_hat_i = self.virtual_clock(time_received)

        name_j = packet['id']
        alpha_hat_j = packet['alpha_hat']
        tau_j = packet['tau']
        tau_j_ref = packet['tau_ref']
        tau_hat_j_ref = packet['tau_hat_ref']
        tau_ij = time_received

        if name_j not in self.ats_data:
            self.ats_data[name_j] = {'eta_ij': 1.0,
                                     'tau_j': tau_j,
                                     'tau_ij': tau_ij, }
        else:
            delta_tau_j = tau_j - self.ats_data[name_j]['tau_j']
            delta_tau_ij = tau_ij - self.ats_data[name_j]['tau_ij']

            if delta_tau_ij == 0:
                # we don't plan on catching this exception, but we want
                # to make sure that when we run the test it never happens.
                # it __shouldn't__ ever happen.
                raise ZeroDivisionError('delta_tau_ij is zero')

            eta_ij = (self.rho_eta*self.ats_data[name_j]['eta_ij'] +
                     (1 - self.rho_eta)*(delta_tau_j/delta_tau_ij))

            self.alpha_hat_i = (self.rho_alpha*self.alpha_hat_i +
                               (1 - self.rho_alpha)*eta_ij*alpha_hat_j)

            self.tau_hat_i_ref = (
                self.rho_nu * (self.alpha_hat_i * (tau_ij - self.tau_i_ref) +
                self.tau_hat_i_ref) + (1 - self.rho_nu) * (alpha_hat_j *
                (tau_j - tau_j_ref) + tau_hat_j_ref))

            self.tau_hat_i_ref = tau_ij
            self.ats_data[name_j] = {'eta_ij': eta_ij,
                                     'tau_j': tau_j,
                                     'tau_ij': tau_ij, }

    def virtual_clock(self, clock):
        virt_time = (self.alpha_hat_i*(clock -
                     self.tau_i_ref) + self.tau_hat_i_ref)
        return int(floor(virt_time))

    def log(self, error=0):
        return [self.id, self.virtual_clock(self.clock.time()),
                self.alpha_hat_i, error, ClockManager.simulation_time()]
