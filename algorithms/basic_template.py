from __future__ import division
from netcloxync.framework.managers import ClockManager, EventManager, NetworkManager
from math import floor
# import numpy as np


### Examples of using the framework.
# 1a. Queue a timed event
#       self.event_manager.add_timed_event(self.sync_int, self.my_event_name)
# 1b. Handling a timed event.
#   def my_event_name(self):
#       # my_event_name work.
# 2. Transmit a packet to neighbours.        
#       packet = {'mesgpt1':'part1', 'mesgpt2':'part2'}
#       for n in self.net_manager.neighbours():
#           self.net_manager.send_packet(n, packet)
# 3. Get current timestamp.
#       timestamp = self.clock.time()
# 4. Get Virtual Time. (assuming virtual_clock() method implemented)
#       virtualtime = virtual_clock(timestamp)

class MyAlgorithmName(object):
    # If algorithm requires more parameters, insert more offset.
    def __init__(self, nodeid, skew, offset):
        self.id = nodeid
        self.clock = ClockManager(skew, offset)
        self.net_manager = NetworkManager(self, nodeid)
        self.event_manager = EventManager(self.clock)

        # 1. Store Alg Parameters if required.
        # 2. Perform Initialisation work.

    def recv_packet(self, packet):
        # if example send occured, can receive the message parts as follows
        # mesg1 = packet['mesgpt1']
        # mesg2 = packet['mesgpt2']

    # A location to implement a virtual clock.
    def virtual_clock(self, localtime):
        # vclock = some expression
        return int(floor(vclock))

    # The log function is called by the test fixture and is used to dump data
    # to be plotted after the simulation. 
    def log(self):
        # the following example presumes the estimated skew is stored in self.alpha_hat_i
#       return [self.id, self.virtual_clock(), self.alpha_hat_i, ClockManager.simulation_time()]