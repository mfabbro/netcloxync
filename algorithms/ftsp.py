from __future__ import division
from netcloxync.framework.managers import ClockManager
from netcloxync.framework.managers import EventManager
from netcloxync.framework.managers import NetworkManager
from collections import deque
from math import floor
import numpy as np


class FloodingTimeSync(object):
    algname = 'FloodingTimeSync'

    def __init__(self, nodeid, skew, offset, **kwargs):
        self.id = nodeid
        self.clock = ClockManager(skew, offset)
        self.net_manager = NetworkManager(self, nodeid)
        self.event_manager = EventManager(self.clock)

        # Algorithm Parameters as initialised by the test.
        self.sync_int = kwargs['sync_int']
        self.num_entries_limit = kwargs['num_entries_limit']
        self.root_timeout = kwargs['root_timeout']
        self.time_error_limit = kwargs['time_error_limit']

        # Create any instance variables needed for the synchronisation alg.
        self.myid = nodeid
        self.my_root_id = nodeid
        self.alpha_hat = 1.0
        self.omicron_hat = 0
        self.heartbeats = 0
        self.highest_sequence_number = 0
        self.regression_table = deque(maxlen=self.num_entries_limit)

        # Start FTSP Algorithm
        self.event_manager.add_timed_event(self.sync_int, self.sync_int_expired)

    def sync_int_expired(self):
        """
        """
        self.heartbeats += 1

        if (self.my_root_id != self.myid) and (self.heartbeats >= self.root_timeout):
            self.my_root_id = self.myid

        if (len(self.regression_table) >= self.num_entries_limit) or (self.my_root_id == self.myid):
            ftsp_packet = {'time_stamp': self.virtual_clock(self.clock.time()),
                           'root_id': self.my_root_id,
                           'seq_num': self.highest_sequence_number, }
        for n in self.net_manager.neighbours():
            self.net_manager.send_packet(n, ftsp_packet)

        if (self.my_root_id == self.myid):
            self.highest_sequence_number += 1

        self.event_manager.add_timed_event(self.sync_int, self.sync_int_expired)

    def recv_packet(self, packet):
        time_received = self.clock.time()
        # change the words tau_hat to something more intuitive.
        tau_hat = self.virtual_clock(time_received)

        if (packet['root_id'] < self.my_root_id):
            self.my_root_id = packet['root_id']
        elif (packet['root_id'] > self.my_root_id) or (packet['seq_num'] <= self.highest_sequence_number):
            return

        # Here if packet['root_id'] <= self.my_root_id and packet['seq_num'] > self.highest_sequence_number
        self.highest_sequence_number = packet['seq_num']
        if (self.my_root_id < self.myid):
            self.heartbeats = 0

        error = abs(tau_hat - packet['time_stamp'])
        if (len(self.regression_table) >= self.num_entries_limit) and error > self.time_error_limit:
            self.regression_table.clear()
        else:
            self.add_entry_and_perform_regression(time_received, packet['time_stamp'])

    def add_entry_and_perform_regression(self, time_stamp, time_received):
        self.regression_table.append((time_stamp, time_received))
        if len(self.regression_table) > 2:
            # regressiontable = [(ts1, tr1), (ts2, tr2), ... ]
            # use zip to grab ts = [ts1, ts2, ts3, ...] and tr = [tr1, tr2, tr3, ...]
            ts, tr = zip(*self.regression_table)
            # Construct W = [ ts1  1; ts2 1; ts3 1; ... ]
            W = zip(ts, [1]*len(ts))

            # Convert structures into np.array structures so that matrix multiplication
            # and psuedo inverse functions can be called on them.
            W = np.array(W)
            y = np.array(tr)

            # y = Wx, Now solve for x = W.
            x = np.dot(np.linalg.pinv(W), y)
            self.alpha_hat, self.omicron_hat = x

    def virtual_clock(self, clock):
        return int(floor(self.alpha_hat*clock + self.omicron_hat))

    def log(self, error=0):
        return [self.id, self.virtual_clock(self.clock.time()), self.alpha_hat, error, ClockManager.simulation_time()]
