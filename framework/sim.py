from __future__ import division
from math import floor
from netcloxync.framework.managers import ClockManager, EventManager, NetworkManager
import csv, os

def ensure_dir(f):
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)

def sim_netcloxync(tname, sim_hz, total_simtime, nodes, delay_func=None, nettop=None):
    print 'Simulation Start'
    NetworkManager.initialize_network(delay_func)
    if nettop is None:
        NetworkManager.topology('completely_connected')
        next_switch = total_simtime + 1  # never switch.
    else:
        top, next_switch = nettop.get_topology()
        NetworkManager.topology('gfunc', top)

    logfile = './log/%s/%s.csv' % (nodes[0].algname, tname)
    ensure_dir(logfile)
    with open(logfile, 'wb') as csvfile:
        logwriter = csv.writer(csvfile)
        while (ClockManager.simulation_time() < total_simtime):
            next_step = EventManager.next_simulation_time()
            ClockManager.set_simulation_time(next_step)
            while ClockManager.simulation_time() > next_switch:
                if next_switch == -1:
                    next_switch = total_simtime + 1  # no more switching.
                else:
                    print 'Changing Topology at %.2fs' % (ClockManager.simulation_time()/sim_hz)
                    top, next_switch = nettop.get_topology()
                    NetworkManager.topology('gfunc', top)
            EventManager.process_events(next_step)
            vclks = [n.virtual_clock(n.clock.time()) for n in nodes]
            avgclk = int(floor(sum(vclks)/len(vclks)))
            for node in nodes:
                error = node.virtual_clock(node.clock.time()) - avgclk
                logwriter.writerow(node.log(error))
    print 'Simulation Complete'
