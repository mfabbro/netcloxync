from nose.tools import assert_equal, assert_raises, assert_in, assert_not_in
from math import floor
from netcloxync.framework.managers import ClockManager


class TestClockManager(object):
    def test___init__(self):
        ClockManager.initialize(10e6)
        clkmanloc = ClockManager(1.0, 10000, None, 'local_osc', None)
        clkmanvir = ClockManager(2.0, 20000, None, 'virtual', clkmanloc)

        assert_in(clkmanloc, ClockManager._local_osc_clocks)
        assert_not_in(clkmanloc, ClockManager._virtual_clocks)
        assert_in(clkmanvir, ClockManager._virtual_clocks)
        assert_not_in(clkmanvir, ClockManager._local_osc_clocks)
        assert_raises(RuntimeError, ClockManager, 2.0, 10000, 'notNone', 'local_osc', None)

    # def test___repr__(self):
    #     # clock_manager = ClockManager(nom_hz, skew, offset, drift, clktype, guide_clock)
    #     # self.assertEqual(expected, clock_manager.__repr__())
    #     assert False  # TODO: implement your test here

    # def test___str__(self):
    #     # clock_manager = ClockManager(nom_hz, skew, offset, drift, clktype, guide_clock)
    #     # self.assertEqual(expected, clock_manager.__str__())
    #     assert False  # TODO: implement your test here

    def test_advance_time(self):
        ClockManager.initialize(10e6)
        clkmanloc = ClockManager(0.5, 10e3, None, 'local_osc', None)
        clkmanvir = ClockManager(2.0, 20e3, None, 'virtual', clkmanloc)

        assert_equal(clkmanloc.time(), 10e3)
        assert_equal(clkmanvir.time(), 20e3)

        clkmanloc.advance_time(10e6)
        clkmanvir.advance_time(10e6)
        clkmanloc.advance_time(10e6)
        clkmanvir.advance_time(10e6)

        assert_equal(clkmanloc._time, 10e3 + 0.5*10e6 + 0.5*10e6)
        assert_equal(clkmanvir._time, 20e3 + 0.5*10e6*2.0 + 0.5*10e6*2.0)

    def test_clean_up(self):
        ClockManager.initialize(10e6)
        clkmanloc = ClockManager(0.5, 10e3, None, 'local_osc', None)
        clkmanvir = ClockManager(2.0, 20e3, None, 'virtual', clkmanloc)

        assert_in(clkmanloc, ClockManager._local_osc_clocks)
        assert_not_in(clkmanloc, ClockManager._virtual_clocks)
        assert_in(clkmanvir, ClockManager._virtual_clocks)
        assert_not_in(clkmanvir, ClockManager._local_osc_clocks)

        ClockManager.clean_up()
        assert_not_in(clkmanloc, ClockManager._local_osc_clocks)
        assert_not_in(clkmanvir, ClockManager._virtual_clocks)

    def test_future_time(self):
        ClockManager.initialize(10e6)
        clkmanloc = ClockManager(0.5, 10e3, None, 'local_osc', None)
        clkmanvir = ClockManager(2.0, 20e3, None, 'virtual', clkmanloc)
        assert_equal(clkmanloc.future_time(1000), 10e6/0.5)
        assert_equal(clkmanvir.future_time(1000), 10e6/(0.5*2.0))

    def test_time(self):
        """
        This test isn't checking rounding.
        """
        ClockManager.initialize(10e6)
        clkmanloc = ClockManager(0.5, 10e3, None, 'local_osc', None)
        clkmanloc.advance_time(10e6)
        assert_equal(clkmanloc.time(), 5e6+10e3)

    def test_set_simulation_time(self):
        ClockManager.initialize(10e6)
        clkmanloc = ClockManager(0.5, 10e3, None, 'local_osc', None)
        clkmanvir = ClockManager(2.0, 20e3, None, 'virtual', clkmanloc)

        ClockManager.set_simulation_time(9999)
        assert_equal(clkmanloc.time(), int(floor(10e3 + 0.5*9999)))
        assert_equal(clkmanvir.time(), int(floor(20e3 + 9999)))

    def test_simulation_time(self):
        ClockManager.initialize(10e6)
        ClockManager(0.5, 10e3, None, 'local_osc', None)

        ClockManager.set_simulation_time(9999)
        assert_equal(ClockManager.simulation_time(), 9999)
