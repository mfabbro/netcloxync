from nose.tools import assert_equal, assert_raises, assert_in, assert_not_in
from netcloxync.framework.managers import ClockManager, EventManager, NetworkManager


class TestFailed(Exception):
    pass


class NetworkedNode(object):

    def __init__(self, name):
        self.name = name
        self.recvd_packets = []

    def recv_packet(self, mesg):
        name = self.name
        if (mesg['par1-'+name] == 'par1' and mesg['par2-'+name] == 'par2'):
            self.recvd_packets.append(mesg)
        else:
            raise TestFailed()


class TestNetworkManager(object):
    def test_clean_up(self):
        NetworkManager._node_data = {'1': 1, '2': 2, '3': 3}
        NetworkManager.clean_up()
        assert_equal(NetworkManager._node_data, {})

    def test___init__(self):
        node = NetworkedNode('nodeA')
        NetworkManager(node, 'nodeA')

        assert_in('nodeA', NetworkManager._node_data)
        assert_raises(LookupError, NetworkManager, node, 'nodeA')

    def test_dump(self):
        """
        This Test does nothing but shows that the function does not fail.
        The stdout can be checked manually by calling nodetests -s ...
        """
        NetworkManager.dump()
        assert True

    # def test_initialize_network(self):
    #     delay_func = lambda : randrange(1, 10)
    #     NetworkManager.initialize_network(delay_func)

    #     assert_equal(NetworkManager._delay_func, delay_func)
    #     assert_equal(NetworkManager._drop_percentage, 0)

    def test_topology_and_neighbours(self):
        n1 = NetworkedNode('n1')
        n2 = NetworkedNode('n2')
        n3 = NetworkedNode('n3')

        NetworkManager.clean_up()
        netman1 = NetworkManager(n1, 'n1')
        netman2 = NetworkManager(n2, 'n2')
        netman3 = NetworkManager(n3, 'n3')

        NetworkManager.initialize_network()
        # Completely Connected Test
        NetworkManager.topology('completely_connected')

        assert_equal(netman1.neighbours(), ['n2', 'n3'])
        assert_equal(netman2.neighbours(), ['n1', 'n3'])
        assert_equal(netman3.neighbours(), ['n1', 'n2'])

        # Custom topology test.
        custom_topology = {'n1': ['n2'],
                           'n2': ['n3'],
                           'n3': ['n1'], }
        NetworkManager.topology('custom_topology', custom_topology)
        assert_equal(netman1.neighbours(), ['n2'])
        assert_equal(netman2.neighbours(), ['n3'])
        assert_equal(netman3.neighbours(), ['n1'])

        # Custom topology: bad topology 1
        custom_topology = {'n1': ['n2'],
                           'fakename': ['n3'],
                           'n3': ['n1'], }
        assert_raises(LookupError, NetworkManager.topology, 'custom_topology', custom_topology)

        # Custom topology: bad topology 2
        custom_topology = {'n1': ['n2'],
                           'n2': ['fakename'],
                           'n3': ['n1'], }
        assert_raises(LookupError, NetworkManager.topology, 'custom_topology', custom_topology)

        # Custom_File Test
        assert_raises(ValueError, NetworkManager.topology, 'custom_file')

        # Unknown top_type test.
        assert_raises(ValueError, NetworkManager.topology, 'bogus_topology')

    def test_send_packet(self):
        EventManager.clean_up()
        ClockManager.clean_up()
        ClockManager.initialize(10e6)

        NetworkManager.clean_up()
        n1 = NetworkedNode('n1')
        n2 = NetworkedNode('n2')
        n3 = NetworkedNode('n3')

        netman1 = NetworkManager(n1, 'n1')
        netman2 = NetworkManager(n2, 'n2')
        netman3 = NetworkManager(n3, 'n3')
        NetworkManager.initialize_network()
        NetworkManager.topology('completely_connected')

        packet1 = {'par1-n2': 'par1', 'par2-n2': 'par2'}
        netman1.send_packet('n2', packet1)

        packet2 = {'par1-n3': 'par1', 'par2-n3': 'par2'}
        netman1.send_packet('n3', packet2)

        packet3 = {'par1-n1': 'par1', 'par2-n1': 'par2'}
        netman2.send_packet('n1', packet3)

        packet4 = {'par1-n1': 'par1', 'par2-n1': 'par2'}
        netman3.send_packet('n1', packet4)

        next_step = EventManager.next_simulation_time()
        while next_step is not None:
            ClockManager.set_simulation_time(next_step)
            EventManager.process_events(next_step)
            next_step = EventManager.next_simulation_time()

        assert_not_in(packet1, n1.recvd_packets)
        assert_not_in(packet2, n1.recvd_packets)
        assert_in(packet3, n1.recvd_packets)
        assert_in(packet4, n1.recvd_packets)

        assert_in(packet1, n2.recvd_packets)
        assert_not_in(packet2, n2.recvd_packets)
        assert_not_in(packet3, n2.recvd_packets)

        assert_not_in(packet1, n3.recvd_packets)
        assert_in(packet2, n3.recvd_packets)
        assert_not_in(packet3, n3.recvd_packets)

        assert_raises(LookupError, netman3.send_packet, 'fakename', packet4)
