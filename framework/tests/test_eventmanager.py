from nose.tools import assert_equal, assert_in, nottest
from netcloxync.framework.managers import ClockManager, EventManager
from blist import sortedlist
from pickle import loads as pickle_loads


class TestEventManager(object):
    _process_events_check = []

    def test___init__(self):
        ClockManager.initialize(10e6)
        clkmanloc = ClockManager(0.5, 10e3, None, 'local_osc', None)
        ev_man = EventManager(clkmanloc)
        assert_equal(ev_man._clock, clkmanloc)

    def test_clean_up(self):
        ClockManager.initialize(10e6)
        EventManager(ClockManager(0.5, 10e3, None, 'local_osc', None))
        EventManager._new_events = [1, 2, 3, 4, 5]
        EventManager._event_magazine = [1, 2, 3, 4, 5]
        EventManager.clean_up()

        assert_equal(EventManager._new_events, [])
        assert_equal(EventManager._event_magazine, sortedlist())

    def test_add_timed_event(self):
        ClockManager.initialize(10e6)
        evman1 = EventManager(ClockManager(1.0, 10e3, None, 'local_osc', None))
        evman2 = EventManager(ClockManager(2.0, 10e3, None, 'local_osc', None))

        evman1.add_timed_event(1, sum, [1, 2])
        evman2.add_timed_event(1, sum, [3, 4], [5, 6])

        simtime, method, pickledopts = EventManager._new_events[0]
        opts = pickle_loads(pickledopts)
        assert_equal(simtime, 10e6/1000)
        assert_equal(method, sum)
        assert_equal(opts[0], [1, 2])

        simtime, method, pickledopts = EventManager._new_events[1]
        opts = pickle_loads(pickledopts)
        assert_equal(simtime, (10e6/1000)*0.5)
        assert_equal(method, sum)
        assert_equal(opts[0], [3, 4])
        assert_equal(opts[1], [5, 6])

    def test_next_simulation_time(self):
        ClockManager.initialize(10e6)
        evman1 = EventManager(ClockManager(1.0, 10e3, None, 'local_osc', None))
        evman2 = EventManager(ClockManager(2.0, 10e3, None, 'local_osc', None))

        assert_equal(EventManager.next_simulation_time(), None)

        evman1.add_timed_event(1, sum, [1, 2])
        evman2.add_timed_event(1, sum, [3, 4], [5, 6])

        assert_equal(EventManager.next_simulation_time(), (10e6/1000)*0.5)

    @nottest
    def test_event_one_for_process_events(self, clock, evman, testval):
        print ClockManager.simulation_time(), clock.time(), testval
        if (ClockManager.simulation_time() == 10e3) and (testval == [1, 2]):
            TestEventManager._process_events_check.append('event_one')
        else:
            raise FailedProcessEventsTest('event_one failed')

    @nottest
    def test_event_two_for_process_events(self, clock, evman, testval):
        print ClockManager.simulation_time(), clock.time(), testval
        if (ClockManager.simulation_time() == 5e3) and (testval == [3, 4]):
            TestEventManager._process_events_check.append('event_two')
        else:
            raise FailedProcessEventsTest('event_two failed')

    def test_process_events(self):
        EventManager.clean_up()
        ClockManager.clean_up()
        ClockManager.initialize(10e6)
        clock1 = ClockManager(1.0, 10e3, None, 'local_osc', None)
        clock2 = ClockManager(2.0, 10e3, None, 'local_osc', None)
        evman1 = EventManager(clock1)
        evman2 = EventManager(clock2)

        method1 = self.test_event_one_for_process_events
        method2 = self.test_event_two_for_process_events
        evman1.add_timed_event(1, method1, clock1, evman1, [1, 2])
        evman2.add_timed_event(1, method2, clock2, evman2, [3, 4])

        next_step = EventManager.next_simulation_time()
        ClockManager.set_simulation_time(next_step)
        EventManager.process_events(next_step)

        next_step = EventManager.next_simulation_time()
        ClockManager.set_simulation_time(next_step)
        EventManager.process_events(next_step)

        assert_in('event_one', TestEventManager._process_events_check)
        assert_in('event_two', TestEventManager._process_events_check)
        assert_equal(len(EventManager._event_magazine), 0)
        assert_equal(len(EventManager._new_events), 0)


class FailedProcessEventsTest(Exception):
    pass
