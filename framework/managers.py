from pickle import dumps as pickle_dumps
from pickle import loads as pickle_loads
from math import floor
from blist import sortedlist

class ClockManager:
    """
    ClockManager is the class that simulates the local oscillators and virtual
    clocks of the sensor network. It maintains a list of all the clocks running
    at any one time and by the singleton method +set_simulation_time+ it updates
    all the clocks simultaneously based on how much the standardised 'simulation
    clock' has advanced by.
    """

    @classmethod
    def initialize(cls, nom_hz):
        cls._nom_hz = nom_hz
        cls._simulation_clock = 0
        cls._virtual_clocks = []
        cls._local_osc_clocks = []

    @classmethod
    def simulation_time(cls):
        """
        Returns current simulation time (in clock ticks)
        """
        return int(floor(cls._simulation_clock))

    @classmethod
    def set_simulation_time(cls, next_simulation_time):
        """
        All local_osc and virtual clocks are functions of the
        simulation_time. This function updates all local_osc and
        virtual clocks to the their times corresponding to this new
        simulation_time.
        *next_simulation_time* describes what the simulation should be
        set to
        """
        if next_simulation_time is not None:
            # First advance all local osc.
            for clk in cls._local_osc_clocks:
                clk.advance_time(next_simulation_time - cls._simulation_clock)

            # Virtual clocks are dependant on local oscs, update them now.
            for clk in cls._virtual_clocks:
                clk.advance_time(next_simulation_time - cls._simulation_clock)

            # Set new simulation clock time.
            cls._simulation_clock = next_simulation_time

    @classmethod
    def clean_up(cls):
        """Resets the ClockManager"""
        cls._simulation_clock = 0
        cls._local_osc_clocks = []
        cls._virtual_clocks = []

    # Instance Methods -
    ##
    # ==== Attributes
    # * +skew+   - the rate this clock instance updates its ticks compared to
    #   the simulation clock.
    # * +offset+ - the total number of ticks this clock is initialised with at
    #   simulation time 0.
    # * +drift+  - not currently implemented. In the future will describe how
    #   the skew of the local_oscillator clock types change with time.
    # * +clktype+   - can either be +'local_osc'+ if the clock represents the local
    #   oscillator of a sensor; or, +'virtual'+, if the clock instance is tied to
    #   another clock rather than directly to the simulation time.
    # * +guide_clock+ - if +'virtual'+ was selected for the +clktype+ then this
    #   input must include a clock_instance that the virtual clock operates in
    #   relation to.
    # --
    def __init__(self, skew=1, offset=0, drift=None, clktype='local_osc',
                 guide_clock=None):
        self._clktype = clktype
        self._guide_clock = guide_clock
        self.skew = skew
        self.offset = offset
        self._time = offset

        if drift is not None:
            raise RuntimeError('Drift is not currenty supported by ClockManager')
            """
            TODO: Implement Drift Later
            self.drift = lambda x: return 0
            self.drift_solve = lambda h,skew: h/skew
            """
        if clktype is 'local_osc':
            ClockManager._local_osc_clocks.append(self)
            self._guide_clock = None
        elif clktype is 'virtual':
            ClockManager._virtual_clocks.append(self)
            self._prev_guide_clock_time = guide_clock._time
            self._guide_clock = guide_clock
        else:
            raise ValueError('Unsupported clktype: must be "local_osc" or "virtual", received "%s"' % clktype)

    def __repr__(self):
        return "ClockManager(%r,%r,%r,%r,%r)" % (ClockManager._nom_hz, self.skew, self.offset, self._clktype, self._guide_clock)

    def __str__(self):
        return "ClockManager"

    def future_time(self, msec_in_future):
        """
        TODO: Implement Drift. Problem, need to solve for the following,
        Suppose our local osc O is a function of simulation time t
        O(t) = offset + skew*t + g(t)
        where g(t) = int_0^t int_0^h drift_func(h) dydh
        Where the drift func represents how the skew drifts in time.

        We need to solve a for a root to determine dt, we need
        to find dt such that
        g(t + dt) + skew*dt - g(t) - msec_in_future = 0

        since g(t) is nonlinear we cant just solve for dt, need to do some
        numerical method which is annoying.
        """
        if self._clktype == 'virtual':
            skew = self.skew * self._guide_clock.skew
        else:
            skew = self.skew

        inc_simtime = msec_in_future * (ClockManager._nom_hz / 1000)/skew

        return (ClockManager._simulation_clock + inc_simtime)

    def advance_time(self, sim_clockticks):
        if self._clktype is 'local_osc':
            self._time += sim_clockticks*self.skew
        elif self._clktype is 'virtual':
            self._time += (self._guide_clock._time - self._prev_guide_clock_time)*self.skew
            self._prev_guide_clock_time = self._guide_clock._time
        else:
            raise ValueError('Invalid "_clktype" should be "local_osc", or "virtual"')

    def time(self):
        return int(floor(self._time))


class EventManager:
    """Manages the event datastructure and dispatch"""
    # Each event contains (time, method, opts)
    # Hence, sort by time.
    _event_magazine = sortedlist(key=lambda tup: tup[0])
    _new_events = []

    @classmethod
    def next_simulation_time(cls):
        cls._event_magazine.update(cls._new_events)
        cls._new_events = []

        if len(cls._event_magazine) > 0:
            return cls._event_magazine[0][0]  # return the time of the closest event
        else:
            return None

    @classmethod
    def clean_up(cls):
        cls._event_magazine = sortedlist(key=lambda tup: tup[0])
        cls._new_events = []

    def __init__(self, clock):
        self._clock = clock

    def add_timed_event(self, msec_til_event, event_method, *method_opts):
        pickled_opts = pickle_dumps(method_opts)
        simtime = self._clock.future_time(msec_til_event)
        EventManager._new_events.append((simtime, event_method, pickled_opts))

    @classmethod
    def process_events(cls, current_time):
        trigd_event = 0
        for (event_time, event_method, pickled_opts) in cls._event_magazine:
            # if time of event trigger <= current time
            if event_time <= current_time:
                # if time is less, event_method=method ev[2] method opts.
                #print event_method, event_opts
                opts = pickle_loads(pickled_opts)
                event_method(*opts)
                trigd_event += 1
            else:
                # since event_magazine is sorted, the first event after the
                # current time indicates all remaining are after this time.
                break
        # Delete all of the events that were successfully triggered this round.
        del(cls._event_magazine[0:trigd_event])


class NetworkManager:
    """
    NetworkManager is a class that deals with the interconnect between
    different nodes. Once all Nodes have created an instance of
    NetworkManager, the singleton class +topology+ must be called to
    define the specific interconnect between the nodes.
    """
    _node_data = {}
    # _node_data = { node_name : {'neighbours': [neighbour_name, ...],
    #                               'obj': obj_instance}
    #               }

    def __init__(self, client, node_name):
        """
        ==== Attributes
        * +client+ - An object instance that wishes to be connected into the
          network. This object must have the +recv_packet+ method in order to
          receive communications from the NetworkManager.
        * +node_name+ - A String used to uniquely identify this particular node
          in the network. Initialize will raise an error if two identically
          named nodes register to the network manager.
        """
        self.node_name = node_name
        if node_name in NetworkManager._node_data:
            raise LookupError("NetworkManager:%s has already been registered" % (node_name))
        else:
            NetworkManager._node_data[node_name] = {'obj': client, 'neighbours': []}

    def neighbours(self):
        """
        neighbours(): returns an array of all the names of the neighbouring
        nodes in the network.
        """
        return NetworkManager._node_data[self.node_name]['neighbours']

    def send_packet(self, dest_name, mesg):
        """
        The method used to send packets across the simulated network. Only
        neighbours can be transmitted to. At the moment the method will raise an
        exception if a non-neighbour or unknown node is used as the destination.
        ==== Attributes
        * +dest_node_name+ - A String used to identify a neighbouring node whom
        you wish to send a packet to.
        * +mesg+ - The data you wish to send. In order to accurately simulate the
        packet types it would be preferred if the data was serialised.
        neighbours can be transmitted to. At the moment the method will raise an
        """
        wait = NetworkManager._delay_func(self.node_name, dest_name, 
                                          ClockManager._simulation_clock)
        if dest_name in NetworkManager._node_data:
            toaddr = NetworkManager._node_data[dest_name]['obj'].recv_packet
            NetworkManager._event_manager.add_timed_event(wait, toaddr, mesg)
        else:
            raise LookupError("%s is not in _node_data" % dest_name)

    @classmethod
    def zero_ms_delay(cls, i, j, t):
        return 0

    @classmethod
    def initialize_network(cls, delay_func=None, drop_percentage=0.0):
        """
        When the network is initialized, an event manager is used to setup the
        timing for the transmission of packets.
        nom_hz: is the nominal clk frequency for this network packet transmission.
        delay_func: is an function handle that will return the number of ms.
                    that the packet should be delayed to simulate transmission
                    delays.
        drop_percentage: must be a value between 0 and 1 which indicates the
                    percentage chance that a packet is LOST in transmission.
                    (i.e. that packet is not received at the other end of comms)
        """
        cls._drop_percentage = drop_percentage
        cls._event_manager = EventManager(ClockManager())
        if delay_func is None:
            class ZeroDelay(object):
                def get_delay(self, i, j, t):
                    return 0
            cls._delay_func = ZeroDelay().get_delay
            #cls._delay_func = cls.zero_ms_delay
        else:
            cls._delay_func = delay_func.get_delay

    @classmethod
    def clean_up(cls):
        cls._node_data = {}

    @classmethod
    def dump(self):
        print(NetworkManager._node_data)
        print("--")

    @classmethod
    def topology(cls, top_type, options=None):
        """
        This method must be called on NetworkManager after all the nodes have
        registered plus any other time when the network topology should be
        changed. Before this method is called all nodes are disconnected from each
        other which means an inability to send and receive packets.
        ==== Attributes
        * +top_type+ - At the moment only accepts +'completely_connected'+ but in the
          future will take inputs such as :custom.
        * +options+ - If a custom topology is chosen, +options+ must be populated
          with the topology of the network.A topology with two isolated networks
          of two nodes would be:
          options = [['nodeA', 'nodeB'], ['nodeC', 'nodeD']]
        --
        """
        if top_type == 'completely_connected':
            """
            'completely_connected': this connects each node to each other node
            in the system.
            """
            for node in cls._node_data:
                cls._node_data[node]['neighbours'] = [n for n in cls._node_data
                                                      if n != node]
        elif top_type == 'custom_topology':
            """
            'custom_topology': Allows the user to specify a custom topology.
            This is acheived by specifying each node's out-going edges
            within the 'options' parameter. See the following example for
            the appropriate structure.
            EX: options = {'nodeA': ['nodeB', 'nodeC'],
                           'nodeB': ['nodeC'],
                           'nodeC': ['nodeB'],}
            """
            for node, outneighbours in options.iteritems():
                # Check options structure contains valid node names
                if node not in cls._node_data:
                    raise LookupError("'%s' not found in node_data" % node)
                for n in outneighbours:
                    if n not in cls._node_data:
                        raise LookupError("'%s' not found in node_data" % n)
                # End checking code
                cls._node_data[node]['neighbours'] = [n for n in outneighbours if n != node]
        elif top_type == 'gfunc':
            # Assume options is an adjacency matrix.
            adjmat = options
            for row, rowval in enumerate(adjmat):
                cls._node_data[row]['neighbours'] = []
                for col, val in enumerate(rowval):
                    if (val != 0) and (row != col):
                        cls._node_data[row]['neighbours'].append(col)

        elif top_type == 'custom_file':
            # TODO:
            raise ValueError('top_type: custom_file, is not currently implemented.')
        else:
            raise ValueError("top_type: %s, is not supported" % top_type)

"""
if __name__ == "__main__":
    NOM_HZ = 10000
    TOTAL_SIMTIME = 50000

    class TestClass:
        def __init__(self, skew, name):
            self._clk = ClockManager(NOM_HZ, skew, 0, None, 'local_osc', None)
            self._ev_man = EventManager(self._clk)
            self._name = name
            self._ev_man.add_timed_event(1, self.initialize, 'par1', 'par2')

        def initialize(self, p1, p2):
            print "simtime:%s name:%s ev:init clk:%s  p1:%s p2:%s" % (ClockManager.simulation_time(), self._name, self._clk._time, p1, p2)
            self._ev_man.add_timed_event(10, self.body, 'par22', 'par22')

        def body(self, p1, p2):
            print "simtime:%s name:%s ev:body clk:%s  p1:%s p2:%s" % (ClockManager.simulation_time(), self._name, self._clk._time, p1, p2)
            self._ev_man.add_timed_event(10, self.initialize, 'par11', 'par11')

    a = TestClass(1,   't1')
    b = TestClass(2, 't2')

    while ClockManager.simulation_time() < TOTAL_SIMTIME:
        next_step = EventManager.next_simulation_time()
        ClockManager.set_simulation_time(next_step)
        EventManager.process_events(next_step)

    # a = ClockManager(100, 1, 10, None, 'local_osc', None)
    # b = ClockManager(100, 2, 0, None, 'local_osc', None)
    # c = ClockManager(100, 1, 0, None, 'virtual', b)
    # print ClockManager.simulation_time()

    # for ints in range(0, 1000, 100):
    #     ClockManager.set_simulation_time(ints)
    #     print ClockManager.simulation_time(), a._time, b._time,c._time

    # print ClockManager._local_osc_clocks
    # print ClockManager._virtual_clocks
    # repr(a)
    # repr(c)
"""
