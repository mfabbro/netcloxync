from netcloxync.algorithms.ftsp import FloodingTimeSync
from netcloxync.algorithms.ats2 import AverageTimeSync
from netcloxync.framework.managers import ClockManager, EventManager, NetworkManager
from math import pow
import csv

test_name = 'T1_complete_graph'
no_of_sensors = 16
sync_int = 1000  # synchronisation interval
nominal_hz = 10e6  # nominal clock speed for all clocks.
total_simulationtime = nominal_hz*60  # simulate for 300 seconds


    # def __init__(self, nodeid, skew, offset, **kwargs):
    #     GenericNode.__init__(self, skew, offset)

    #     # Algorithm Parameters as initialised by the test.
    #     self.sync_int = kwargs['sync_int']
    #     self.rho_eta = kwargs['rho_eta']
    #     self.rho_lv = kwargs['rho_v']
    #     self.rho_o = kwargs['rho_o']

# Generate Clocks
ClockManager.initialize(nominal_hz)
# clocks = []
# for n in xrange(no_of_sensors):

#     clocks.append((n, skew, offset))

# Create sensors.
sensors = []
ftsp_pars = {'sync_int': sync_int,
             'num_entries_limit': 10,
             'root_timeout': 10,
             'time_error_limit': nominal_hz, }  # If the estimate it out by a second.

sensors.append(FloodingTimeSync(nodeid=1, skew=0.98, offset=10e0, **ftsp_pars))
sensors.append(FloodingTimeSync(nodeid=2, skew=0.99, offset=10e2, **ftsp_pars))
sensors.append(FloodingTimeSync(nodeid=3, skew=1.00, offset=10e3, **ftsp_pars))
sensors.append(FloodingTimeSync(nodeid=4, skew=1.01, offset=10e4, **ftsp_pars))
sensors.append(FloodingTimeSync(nodeid=5, skew=1.02, offset=10e6, **ftsp_pars))

# ats_pars = {'sync_int': sync_int,
#             'rho_eta': 0.7,
#             'rho_v': 0.7,
#             'rho_o': 0.5, }
# sensors.append(AverageTimeSync(nodeid=1, skew=0.98, offset=10e0, **ats_pars))
# sensors.append(AverageTimeSync(nodeid=2, skew=0.99, offset=10e2, **ats_pars))
# sensors.append(AverageTimeSync(nodeid=3, skew=1.00, offset=10e3, **ats_pars))
# sensors.append(AverageTimeSync(nodeid=4, skew=1.01, offset=10e4, **ats_pars))
# sensors.append(AverageTimeSync(nodeid=5, skew=1.02, offset=10e6, **ats_pars))



# sensors = []
# for n in xrange(no_of_sensors):
#     skew = 1.0 + n * pow(-1, n) * 0.01
#     offset = n*100
#     sensors.append(AverageTimeSync(n, skew, offset, **ats_pars))


NetworkManager.initialize_network()
NetworkManager.topology('completely_connected')

with open('./log/' + __file__[:-3] + '.csv', 'wb') as csvfile:
    logwriter = csv.writer(csvfile)
    while (ClockManager.simulation_time() < total_simulationtime):
        next_step = EventManager.next_simulation_time()
        ClockManager.set_simulation_time(next_step)
        EventManager.process_events(next_step)
        for sensor in sensors:
            logwriter.writerow(sensor.log())



# class Fixture(object):
#     def setup(self, fixturename, what):
#         pass

#     def topology(self):
#         pass

#     def run(self):
#         for 
#             while (ClockManager.simulation_time() < self.total_simulation_time):
#                 next_step = EventManager.next_simulation_time()
#                 ClockManager.set_simulation_time(next_step)
#                 EventManager.process_events(next_step)

#             NetworkManager.clean_up()
#             EventManager.clean_up()
#             ClockManager.clean_up()



# class CompleteGraphFixture(GenericFixture):
#     def __init__(self, no_of_nodes):
#         self.no_of_nodes = no_of_nodes
#         self.desc = 'Complete Graph Topology with %d nodes' % no_of_nodes


# CompleteGraphFixture(16, )